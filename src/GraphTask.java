import java.util.*;
import java.util.stream.Collectors;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {
        Graph g = new Graph("G");
//        GraphTask.Vertex tallinn = g.createVertex("Tallinn");
//        GraphTask.Vertex keila = g.createVertex("Keila");
//        GraphTask.Vertex saku = g.createVertex("Saku");
//        GraphTask.Vertex jüri = g.createVertex("Jüri");
//        GraphTask.Vertex kolga = g.createVertex("Kolga");
//        GraphTask.Vertex haapsalu = g.createVertex("Haapsalu");
//        GraphTask.Vertex rapla = g.createVertex("Rapla");
//        GraphTask.Vertex paide = g.createVertex("Paide");
//        GraphTask.Vertex rakvere = g.createVertex("Rakvere");
//        GraphTask.Vertex pärnu = g.createVertex("Pärnu");
//        GraphTask.Vertex viljandi = g.createVertex("Viljandi");
//        GraphTask.Vertex tartu = g.createVertex("Tartu");
//        GraphTask.Vertex jõhvi = g.createVertex("Jõhvi");
//        g.createEdge(tallinn, saku, 20);
//        g.createEdge(tallinn, jüri, 20);
//        g.createEdge(tallinn, paide, 60);
//        g.createEdge(saku, keila, 35);
//        g.createEdge(saku, rapla, 40);
//        g.createEdge(jüri, kolga, 36);
//        g.createEdge(jüri, rapla, 70);
//        g.createEdge(jüri, paide, 50);
//        g.createEdge(keila, haapsalu, 45);
//        g.createEdge(keila, pärnu, 120);
//        g.createEdge(kolga, rakvere, 25);
//        g.createEdge(haapsalu, pärnu, 100);
//        g.createEdge(rapla, paide, 130);
//        g.createEdge(paide, rakvere, 66);
//        g.createEdge(paide, pärnu, 140);
//        g.createEdge(paide, tartu, 100);
//        g.createEdge(rakvere, jõhvi, 89);
//        g.createEdge(pärnu, viljandi, 99);
//        g.createEdge(viljandi, tartu, 78);
//        g.createEdge(jõhvi, tartu, 35);
        g.createRandomSimpleGraph(2000, 2000);
        System.out.println(g);
        long start = System.currentTimeMillis();
        System.out.println(g.getTwoVerticesWhereShortestPathHasTheMostCities());
        long end = System.currentTimeMillis();
        System.out.println(end / 1000 - start / 1000);
    }

    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private Integer info = 0;
        private Vertex vObject;
        private int cities;
        // You can add more fields, if needed

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        public Iterator outArcs() {
            ArrayList<Arc> arcs = new ArrayList<>();
            if (first == null) return arcs.iterator();
            Arc arc = first;
            arcs.add(first);
            while (arc.next != null) {
                arcs.add(arc.next);
                arc = arc.next;
            }
            return arcs.iterator();
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append("(");
                    sb.append(a.info);
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        public Arc createArc(Vertex from, Vertex to, int distance) {
            Arc res = new Arc("");
            res.info = distance;
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        public void createEdge(Vertex from, Vertex to, int distance) {
            createArc(from, to, distance);
            createArc(to, from, distance);
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some rndom existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Shortest paths from a given vertex. Uses Dijkstra's algorithm.
         * For each vertex vInfo is length of shortest path from given
         * source s and vObject is previous vertex from s to this vertex.
         *
         * @param s source vertex
         */
        public void shortestPathsFrom(Vertex s) {
            if (getVertexList().isEmpty()) return;
            if ((!getVertexList().contains(s))) throw new RuntimeException("wrong argument to Dijkstra!");
            int INFINITY = Integer.MAX_VALUE; // big enough!!!
            Iterator<Vertex> vit = getVertexList().iterator();
            while (vit.hasNext()) {
                Vertex v = vit.next();
                v.info = INFINITY;
                v.vObject = null;
                v.cities = 0;
            }
            s.info = 0;
            List<Vertex> vq = Collections.synchronizedList(new LinkedList<>());
            vq.add(s);
            while (vq.size() > 0) {
                int minLen = INFINITY;
                Vertex minVert = null;
                Iterator it = vq.iterator();
                while (it.hasNext()) {
                    Vertex v = (Vertex) it.next();
                    if (v.info < minLen) {
                        minVert = v;
                        minLen = v.info;
                    }
                }
                vq.remove(minVert);
                if (minVert == null) throw new RuntimeException("Dijkstra error!");
                it = minVert.outArcs();
                while (it.hasNext()) {
                    Arc a = (Arc) it.next();
                    int newLen = minLen + a.info;
                    Vertex to = a.target;
                    if (to.info == INFINITY) {
                        vq.add(to);
                    }
                    if (newLen < to.info) {
                        to.info = newLen;
                        to.vObject = minVert;
                    }
                }
            }
            for (Vertex v : getVertexList()) {
                if (v.vObject == null) {
                    System.out.println("hui");
                    v.info = null;
                    continue;
                }
                Vertex chainVertex = v.vObject;
                while (chainVertex != s && chainVertex != null) {
                    v.cities++;
                    chainVertex = chainVertex.vObject;
                }
            }
        }

        /**
         Put all vertices into ArrayList
         */
        private ArrayList<Vertex> getVertexList() {
            ArrayList<Vertex> vertices = new ArrayList<>();
            Vertex vertex = first;
            vertices.add(first);
            while (vertex.next != null) {
                vertices.add(vertex.next);
                vertex = vertex.next;
            }
            return vertices;
        }

        /**
         * Find two vertices where shortest path has the most cities.
         * Print all paths with distances between all cities.
         * Finally print path or paths required in the exercise
         */
        public String getTwoVerticesWhereShortestPathHasTheMostCities() {
            ArrayList<Vertex[]> results = new ArrayList<>();
            ArrayList<Vertex> ends = (ArrayList<Vertex>) getVertexList().clone();
            int cities = 0;
            for (Vertex start : getVertexList()) {
                ends.remove(start);
                shortestPathsFrom(start);
                for (Vertex end : ends) {
                    if (end.cities > cities) {
                        results = new ArrayList<>();
                        cities = end.cities;
                    }
                    if (end.cities >= cities && end.cities > 0) results.add(new Vertex[]{start, end});
                    System.out.println(String.format("Between %s and %s (%s km) there are %s cities!", start, end, end.info, end.cities));
                }
            }
            return String.format("Path between %s has the biggest amount of cities on the way: %s.", results.stream().map(a -> a[0].id + " and " + a[1].id).collect(Collectors.joining(", ")), cities);
        }
    }
}
